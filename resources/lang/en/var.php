<?php

return [
    "auth" => [
        "login" => "Login",
        "forgot_password" => "Forgot Password",
        "email_or_phonenumber" => "E-mail or phone number",
        "password" => "Password"
    ]
];
