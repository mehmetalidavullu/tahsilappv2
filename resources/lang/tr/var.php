<?php

return [
    "auth" => [
        "login" => "Giriş",
        "forgot_password" => "Şifremi Unuttum",
        "email_or_phonenumber" => "E-posta veya telefon numarası",
        "password" => "Şifre"
    ]
];
