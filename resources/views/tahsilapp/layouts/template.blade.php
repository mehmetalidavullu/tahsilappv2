<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
	<head>

		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta content="Admitro - Admin Panel HTML template" name="description">
		<meta content="Spruko Technologies Private Limited" name="author">
		<meta name="keywords" content="admin panel ui, user dashboard template, web application templates, premium admin templates, html css admin templates, premium admin templates, best admin template bootstrap 4, dark admin template, bootstrap 4 template admin, responsive admin template, bootstrap panel template, bootstrap simple dashboard, html web app template, bootstrap report template, modern admin template, nice admin template"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Title -->
		<title>Admitro - Admin Panel HTML template</title>

		<!--Favicon -->
		<link rel="icon" href="{{ asset("assets/images/brand/favicon.ico") }}" type="image/x-icon"/>

		<!--Bootstrap css -->
		<link href="{{ asset("assets/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">

		<!-- Style css -->
		<link href="{{ asset("assets/css/style.css") }}" rel="stylesheet" />
		<link href="{{ asset("assets/css/dark.css") }}" rel="stylesheet" />
		<link href="{{ asset("assets/css/skin-modes.css") }}" rel="stylesheet" />

		<!-- Animate css -->
		<link href="{{ asset("assets/css/animated.css") }}" rel="stylesheet" />

		<!--Sidemenu css -->
       <link href="{{ asset("assets/css/sidemenu.css") }}" rel="stylesheet">

		<!-- P-scroll bar css-->
		<link href="{{ asset("assets/plugins/p-scrollbar/p-scrollbar.css") }}" rel="stylesheet" />

		<!---Icons css-->
		<link href="{{asset("assets/css/icons.css")}}" rel="stylesheet" />

		<!-- Simplebar css -->
		<link rel="stylesheet" href="{{ asset("assets/plugins/simplebar/css/simplebar.css") }}">

		<!-- INTERNAL Select2 css -->
		<link href="{{ asset("assets/plugins/select2/select2.min.css") }}" rel="stylesheet" />

	    <!-- Color Skin css -->
        <link id="theme" href="{{ asset("assets/colors/color1.css") }}" rel="stylesheet" type="text/css"/>

        @yield('head')

	</head>

	<body class="app sidebar-mini">

		<!---Global-loader-->
		<div id="global-loader" >
			<img src="{{ asset("assets/images/svgs/loader.svg") }}" alt="loader">
		</div>
		<!--- End Global-loader-->

		<!-- Page -->
		<div class="page">
			<div class="page-main">

				<!--aside open-->
				<aside class="app-sidebar">
					<div class="app-sidebar__logo">
						<a class="header-brand" href="index.html">
							<img src="{{ asset("assets/images/brand/logo.png") }}" class="header-brand-img desktop-lgo" alt="Admitro logo">
							<img src="{{ asset("assets/images/brand/logo1.png") }}" class="header-brand-img dark-logo" alt="Admitro logo">
							<img src="{{ asset("assets/images/brand/favicon.png") }}" class="header-brand-img mobile-logo" alt="Admitro logo">
							<img src="{{ asset("assets/images/brand/favicon1.png") }}" class="header-brand-img darkmobile-logo" alt="Admitro logo">
						</a>
					</div>
					<div class="app-sidebar__user">
						<div class="dropdown user-pro-body text-center">
							<div class="user-pic">
								<img src="{{ asset("assets/images/users/2.jpg") }}" alt="user-img" class="avatar-xl rounded-circle mb-1">
							</div>
							<div class="user-info">
								<h5 class=" mb-1">{{ user()->name }} <i class="ion-checkmark-circled  text-success fs-12"></i></h5>
								<span class="text-muted app-sidebar__user-name text-sm"> {{ ucFirst(getCurrentUserRole()) }}</span>
							</div>
						</div>
						<div class="sidebar-navs">
							<ul class="nav nav-pills-circle">
								<li class="nav-item" data-placement="top" data-toggle="tooltip" title="Support">
									<a class="icon" href="#" >
										<i class="las la-life-ring header-icons"></i>
									</a>
								</li>
								<li class="nav-item" data-placement="top" data-toggle="tooltip" title="Documentation">
									<a class="icon" href="#">
										<i class="las  la-file-alt header-icons"></i>
									</a>
								</li>
								<li class="nav-item" data-placement="top" data-toggle="tooltip" title="Projects">
									<a href="#" class="icon">
										<i class="las la-project-diagram header-icons"></i>
									</a>
								</li>
								<li class="nav-item" data-placement="top" data-toggle="tooltip" title="Settings">
									<a class="icon" href="#">
										<i class="las la-cog header-icons"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<ul class="side-menu app-sidebar3">
                        <li class="side-item side-item-category mt-4"></li>
                        <li class="slide">
							<a class="side-menu__item"  href="{{ route("tahsilapp.current") }}">
                                <svg class="side-menu__icon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 5v2h-4V5h4M9 5v6H5V5h4m10 8v6h-4v-6h4M9 17v2H5v-2h4M21 3h-8v6h8V3zM11 3H3v10h8V3zm10 8h-8v10h8V11zm-10 4H3v6h8v-6z"/></svg>
                                <span class="side-menu__label">Cariler</span>
                            </a>
                        </li>
                        <li class="slide">
							<a class="side-menu__item"  href="{{ route("tahsilapp.staff") }}">
                                <svg class="side-menu__icon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 5v2h-4V5h4M9 5v6H5V5h4m10 8v6h-4v-6h4M9 17v2H5v-2h4M21 3h-8v6h8V3zM11 3H3v10h8V3zm10 8h-8v10h8V11zm-10 4H3v6h8v-6z"/></svg>
                                <span class="side-menu__label">Personeller</span>
                            </a>
                        </li>
					</ul>
				</aside>
				<!--aside closed-->

				<!-- App-Content -->
				<div class="app-content main-content">
					<div class="side-app">

						<!--app header-->
						<div class="app-header header">
							<div class="container-fluid">
								<div class="d-flex">
									<a class="header-brand" href="index.html">
										<img src="{{ asset("assets/images/brand/logo.png") }}" class="header-brand-img desktop-lgo" alt="Admitro logo">
										<img src="{{ asset("assets/images/brand/logo1.png") }}" class="header-brand-img dark-logo" alt="Admitro logo">
										<img src="{{ asset("assets/images/brand/favicon.png") }}" class="header-brand-img mobile-logo" alt="Admitro logo">
										<img src="{{ asset("assets/images/brand/favicon1.png") }}" class="header-brand-img darkmobile-logo" alt="Admitro logo">
									</a>
									<div class="app-sidebar__toggle" data-toggle="sidebar">
										<a class="open-toggle" href="#">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-align-left header-icon mt-1"><line x1="17" y1="10" x2="3" y2="10"></line><line x1="21" y1="6" x2="3" y2="6"></line><line x1="21" y1="14" x2="3" y2="14"></line><line x1="17" y1="18" x2="3" y2="18"></line></svg>
										</a>
									</div>
									<div class="mt-1">
										<form class="form-inline">
											<div class="search-element">
												<input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" tabindex="1">
												<button class="btn btn-primary-color" type="submit">
													<svg class="header-icon search-icon" x="1008" y="1248" viewBox="0 0 24 24"  height="100%" width="100%" preserveAspectRatio="xMidYMid meet" focusable="false">
														<path d="M0 0h24v24H0V0z" fill="none"/><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
													</svg>
												</button>
											</div>
										</form>
									</div><!-- SEARCH -->
									<div class="d-flex order-lg-2 ml-auto">
										<a href="#" data-toggle="search" class="nav-link nav-link-lg d-md-none navsearch">
											<svg class="header-icon search-icon" x="1008" y="1248" viewBox="0 0 24 24"  height="100%" width="100%" preserveAspectRatio="xMidYMid meet" focusable="false">
												<path d="M0 0h24v24H0V0z" fill="none"/><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
											</svg>
										</a>
										<div class="dropdown   header-fullscreen" >
											<a  class="nav-link icon full-screen-link p-0"  id="fullscreen-button">
												<svg xmlns="http://www.w3.org/2000/svg" class="header-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M10 4L8 4 8 8 4 8 4 10 10 10zM8 20L10 20 10 14 4 14 4 16 8 16zM20 14L14 14 14 20 16 20 16 16 20 16zM20 8L16 8 16 4 14 4 14 10 20 10z"/></svg>
											</a>
										</div>
										<div class="dropdown header-message">
											<a class="nav-link icon" data-toggle="dropdown">
												<svg xmlns="http://www.w3.org/2000/svg" class="header-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M20,2H4C2.897,2,2,2.897,2,4v12c0,1.103,0.897,2,2,2h3v3.767L13.277,18H20c1.103,0,2-0.897,2-2V4C22,2.897,21.103,2,20,2z M20,16h-7.277L9,18.233V16H4V4h16V16z"/><path d="M7 7H17V9H7zM7 11H14V13H7z"/></svg>
												<span class="badge badge-success side-badge">3</span>
											</a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow  animated">
												<div class="dropdown-header">
													<h6 class="mb-0">Messages</h6>
													<span class="badge badge-pill badge-primary ml-auto">View all</span>
												</div>
												<div class="header-dropdown-list message-menu" id="message-menu">
													<a class="dropdown-item border-bottom" href="#">
														<div class="d-flex align-items-center">
															<div class="">
																<span class="avatar avatar-md brround align-self-center cover-image" data-image-src="{{ asset("assets/images/users/1.jpg") }}"></span>
															</div>
															<div class="d-flex">
																<div class="pl-3">
																	<h6 class="mb-1">Jack Wright</h6>
																	<p class="fs-13 mb-1">All the best your template awesome</p>
																	<div class="small text-muted">
																		3 hours ago
																	</div>
																</div>
															</div>
														</div>
													</a>
													<a class="dropdown-item border-bottom">
														<div class="d-flex align-items-center">
															<div class="">
																<span class="avatar avatar-md brround align-self-center cover-image" data-image-src="{{ asset("assets/images/users/2.jpg") }}"></span>
															</div>
															<div class="d-flex">
																<div class="pl-3">
																	<h6 class="mb-1">Lisa Rutherford</h6>
																	<p class="fs-13 mb-1">Hey! there I'm available</p>
																	<div class="small text-muted">
																		5 hour ago
																	</div>
																</div>
															</div>
														</div>
													</a>
													<a class="dropdown-item border-bottom">
														<div class="d-flex align-items-center">
															<div class="">
																<span class="avatar avatar-md brround align-self-center cover-image" data-image-src="{{ asset("assets/images/users/3.jpg") }}"></span>
															</div>
															<div class="d-flex">
																<div class="pl-3">
																	<h6 class="mb-1">Blake Walker</h6>
																	<p class="fs-13 mb-1">Just created a new blog post</p>
																	<div class="small text-muted">
																		45 mintues ago
																	</div>
																</div>
															</div>
														</div>
													</a>
													<a class="dropdown-item border-bottom">
														<div class="d-flex align-items-center">
															<div class="">
																<span class="avatar avatar-md brround align-self-center cover-image" data-image-src="{{ asset("assets/images/users/4.jpg") }}"></span>
															</div>
															<div class="d-flex">
																<div class="pl-3">
																	<h6 class="mb-1">Fiona Morrison</h6>
																	<p class="fs-13 mb-1">Added new comment on your photo</p>
																	<div class="small text-muted">
																		2 days ago
																	</div>
																</div>
															</div>
														</div>
													</a>
													<a class="dropdown-item border-bottom">
														<div class="d-flex align-items-center">
															<div class="">
																<span class="avatar avatar-md brround align-self-center cover-image" data-image-src="{{ asset("assets/images/users/6.jpg") }}"></span>
															</div>
															<div class="d-flex">
																<div class="pl-3">
																	<h6 class="mb-1">Stewart Bond</h6>
																	<p class="fs-13 mb-1">Your payment invoice is generated</p>
																	<div class="small text-muted">
																		3 days ago
																	</div>
																</div>
															</div>
														</div>
													</a>
													<a class="dropdown-item border-bottom">
														<div class="d-flex align-items-center">
															<div class="">
																<span class="avatar avatar-md brround align-self-center cover-image" data-image-src="{{ asset("assets/images/users/7.jpg") }}"></span>
															</div>
															<div class="d-flex">
																<div class="pl-3">
																	<h6 class="mb-1">Faith Dickens</h6>
																	<p class="fs-13 mb-1">Please check your mail....</p>
																	<div class="small text-muted">
																		4 days ago
																	</div>
																</div>
															</div>
														</div>
													</a>
												</div>
												<div class=" text-center p-2 border-top">
													<a href="#" class="">See All Messages</a>
												</div>
											</div>
										</div>
										<div class="dropdown header-notify">
											<a class="nav-link icon" data-toggle="dropdown">
												<svg xmlns="http://www.w3.org/2000/svg" class="header-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13.586V10c0-3.217-2.185-5.927-5.145-6.742C13.562 2.52 12.846 2 12 2s-1.562.52-1.855 1.258C7.185 4.074 5 6.783 5 10v3.586l-1.707 1.707C3.105 15.48 3 15.734 3 16v2c0 .553.447 1 1 1h16c.553 0 1-.447 1-1v-2c0-.266-.105-.52-.293-.707L19 13.586zM19 17H5v-.586l1.707-1.707C6.895 14.52 7 14.266 7 14v-4c0-2.757 2.243-5 5-5s5 2.243 5 5v4c0 .266.105.52.293.707L19 16.414V17zM12 22c1.311 0 2.407-.834 2.818-2H9.182C9.593 21.166 10.689 22 12 22z"/></svg>
												<span class="pulse "></span>
											</a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow  animated">
												<div class="dropdown-header">
													<h6 class="mb-0">Notifications</h6>
													<span class="badge badge-pill badge-primary ml-auto">View all</span>
												</div>
												<div class="notify-menu">
													<a href="#" class="dropdown-item border-bottom d-flex pl-4">
														<div class="notifyimg bg-info-transparent text-info"> <i class="ti-comment-alt"></i> </div>
														<div>
															<div class="font-weight-normal1">Message Sent.</div>
															<div class="small text-muted">3 hours ago</div>
														</div>
													</a>
													<a href="#" class="dropdown-item border-bottom d-flex pl-4">
														<div class="notifyimg bg-primary-transparent text-primary"> <i class="ti-shopping-cart-full"></i> </div>
														<div>
															<div class="font-weight-normal1"> Order Placed</div>
															<div class="small text-muted">5  hour ago</div>
														</div>
													</a>
													<a href="#" class="dropdown-item border-bottom d-flex pl-4">
														<div class="notifyimg bg-warning-transparent text-warning"> <i class="ti-calendar"></i> </div>
														<div>
															<div class="font-weight-normal1"> Event Started</div>
															<div class="small text-muted">45 mintues ago</div>
														</div>
													</a>
													<a href="#" class="dropdown-item border-bottom d-flex pl-4">
														<div class="notifyimg bg-success-transparent text-success"> <i class="ti-desktop"></i> </div>
														<div>
															<div class="font-weight-normal1">Your Admin lanuched</div>
															<div class="small text-muted">1 daya ago</div>
														</div>
													</a>
												</div>
												<div class=" text-center p-2 border-top">
													<a href="#" class="">View All Notifications</a>
												</div>
											</div>
										</div>
										<div class="dropdown profile-dropdown">
											<a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
												<span>
													<img src="{{ asset("assets/images/users/2.jpg") }}" alt="img" class="avatar avatar-md brround">
												</span>
											</a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated">
												<div class="text-center">
													<a href="#" class="dropdown-item text-center user pb-0 font-weight-bold">{{ user()->name }}</a>
													<span class="text-center user-semi-title">Yönetici</span>
													<div class="dropdown-divider"></div>
												</div>
												<a class="dropdown-item d-flex" href="{{ route("tahsilapp.definition") }}">
                                                    <svg class="header-icon mr-3" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                                        <path d="M0 0h24v24H0z" fill="none"></path>
                                                        <path d="M20 2H4c-1.11 0-2 .89-2 2v11c0 1.11.89 2 2 2h4v5l4-2 4 2v-5h4c1.11 0 2-.89 2-2V4c0-1.11-.89-2-2-2zm0 13H4v-2h16v2zm0-5H4V4h16v6z"></path>
                                                    </svg>
													<div class="">Tanımlamalar</div>
                                                </a>
												<a class="dropdown-item d-flex" href="{{ route('tahsilapp.logout') }}">
													<svg class="header-icon mr-3" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><g><rect fill="none" height="24" width="24"/></g><g><path d="M11,7L9.6,8.4l2.6,2.6H2v2h10.2l-2.6,2.6L11,17l5-5L11,7z M20,19h-8v2h8c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2h-8v2h8V19z"/></g></svg>
													<div class="">Çıkış Yap</div>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/app header-->

						@yield("content")

					</div>
				</div>
				<!-- End app-content-->
			</div>


			<!--Footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 text-center">
							Copyright © 2020 <a href="#">Admitro</a>. Designed by <a href="#">Spruko Technologies Pvt.Ltd</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->

		</div>
		<!-- End Page -->

		<!-- Back to top -->
		<a href="#top" id="back-to-top"><i class="fe fe-chevrons-up"></i></a>

		<!-- Jquery js-->
		<script src="{{ asset("assets/js/jquery-3.5.1.min.js") }}"></script>

		<!-- Bootstrap4 js-->
		<script src="{{ asset("assets/plugins/bootstrap/popper.min.js") }}"></script>
		<script src="{{ asset("assets/plugins/bootstrap/js/bootstrap.min.js") }}"></script>

		<!--Othercharts js-->
		<script src="{{ asset("assets/plugins/othercharts/jquery.sparkline.min.js") }}"></script>

		<!-- Circle-progress js-->
		<script src="{{ asset("assets/js/circle-progress.min.js") }}"></script>

		<!-- Jquery-rating js-->
		<script src="{{ asset("assets/plugins/rating/jquery.rating-stars.js") }}"></script>

		<!--Sidemenu js-->
		<script src="{{ asset("assets/plugins/sidemenu/sidemenu.js") }}"></script>

		<!-- P-scroll js-->
		<script src="{{ asset("assets/plugins/p-scrollbar/p-scrollbar.js") }}"></script>
		<script src="{{ asset("assets/plugins/p-scrollbar/p-scroll1.js") }}"></script>
		<script src="{{ asset("assets/plugins/p-scrollbar/p-scroll.js") }}"></script>

		<!--INTERNAL Peitychart js-->
		<script src="{{ asset("assets/plugins/peitychart/jquery.peity.min.js") }}"></script>
		<script src="{{ asset("assets/plugins/peitychart/peitychart.init.js") }}"></script>

		<!--INTERNAL Apexchart js-->
		<script src="{{ asset("assets/js/apexcharts.js") }}"></script>

		<!--INTERNAL ECharts js-->
		<script src="{{ asset("assets/plugins/echarts/echarts.js") }}"></script>

		<!--INTERNAL Chart js -->
		<script src="{{ asset("assets/plugins/chart/chart.bundle.js") }}"></script>
		<script src="{{ asset("assets/plugins/chart/utils.js") }}"></script>

		<!-- INTERNAL Select2 js -->
		<script src="{{ asset("assets/plugins/select2/select2.full.min.js") }}"></script>
		<script src="{{ asset("assets/js/select2.js") }}"></script>

		<!--INTERNAL Moment js-->
		<script src="{{ asset("assets/plugins/moment/moment.js") }}"></script>

		<!-- Simplebar JS -->
		<script src="{{ asset("assets/plugins/simplebar/js/simplebar.min.js") }}"></script>

		<!-- Custom js-->
        <script src="{{ asset("assets/js/custom.js") }}"></script>

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        @yield('tail')

	</body>
</html>
