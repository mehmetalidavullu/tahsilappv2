@extends('tahsilapp.layouts.template')

@section('content')


<style>
    .fa-eye{
        float: right;
        position: absolute;
        right: 25px;
        top: 50%;
    }

    #overlay {
    background: #ffffff;
    color: #666666;
    position: fixed;
    height: 100%;
    width: 100%;
    z-index: 5000;
    top: 0;
    left: 0;
    float: left;
    text-align: center;
    padding-top: 25%;
    opacity: 1;
    }

    #staffContentTable td {
        overflow-wrap: anywhere;
    }

</style>



    @include('tahsilapp.staffs.layouts.staffs')

    <!--current modal form-->
    <!--current modal form-->
    <div class="modal fade bd-example-modal-lg" id="currentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="min-width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TİTLE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route("tahsilapp.currents.store") }}" class="currentModalForm needs-validation" id="cariekle" method="post" accept-charset="utf-8" novalidate >

                    @csrf
                    <div class="modal-body">

                            <div class="form-group has-validation">
                                <label>Personal Adı Soyadı</label>
                                <input type="text" class="form-control" id="name" placeholder="Adı Soyadı" name="NAME" required>
                                <div class="invalid-feedback">
                                    Bu alanı doldurmanız gerekmektedir.
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 has-validation">
                                        <label>Gsm</label>
                                        <input type="tel" class="form-control required number phoneformat" id="gsm" placeholder="Telefon Numarası" name="GSM" required>
                                        <div class="invalid-feedback">
                                            Bu alanı doldurmanız gerekmektedir.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 has-validation">
                                        <label>E-posta</label>
                                        <input type="email" class="form-control required email emailcontrol" id="email" placeholder="E-posta adresi" name="EMAIL" required>
                                        <div class="invalid-feedback">
                                            Bu alanı doldurmanız gerekmektedir.
                                        </div>
                                    </div>
                                    <div class="col-md-6 has-validation" >
                                        <label>Şifre</label>
                                        <input type="password" class="form-control required" id="password" placeholder="Şifresi" name="PASSWORD" autocomplete="off" required> <div class="eyes"><i class="fa fa-eye" onclick="showPassword()"></i></div>
                                        <div class="invalid-feedback">
                                            Bu alanı doldurmanız gerekmektedir.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Personal Durumu</label><br>

                                <label for="">Aktif</label>
                                <input type="radio" name="STATUS" id="statusActive" value="1" checked="">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label>Pasif</label>
                                <input type="radio" name="STATUS" id="statusPasif" value="0">

                            </div>

                            <input type="hidden" id="id"  name="id" value="">

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info pull-right"  onclick="currentSaveBtn()">Kaydet</button>
                    </div>

               </form>
            </div>
          </div>

    </div>
    <!--current modal form END-->
    <!--current modal form END-->

    <!--current Info modal form-->
    <!--current Info modal form-->
    <div class="modal fade dtr-bs-modal" id="currentModalInfo" role="dialog"  aria-modal="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Details for Airi Satou</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <table class="table border mb-0 dtr-details" width="100%">
                        <tr data-dt-row="4" data-dt-column="0">
                            <td>Cari Adı:</td>
                            <td id="tdCariAdi">Airi</td>
                        </tr>
                        <tr data-dt-row="4" data-dt-column="8">
                            <td>Gsm:</td>
                            <td id="tdGSM"></td>
                        </tr>
                        <tr data-dt-row="4" data-dt-column="8">
                            <td>E-posta:</td>
                            <td id="tdMail"></td>
                        </tr>
                        <tr data-dt-row="4" data-dt-column="8">
                            <td>Personal Durumu:</td>
                            <td id="tdBayiDurumu"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--current Info modal form END-->
    <!--current Info modal form END-->

    <!--spinner-->
    <!--spinner-->
    <div id="overlay" style="display:none;">
        <div class="spinner4">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!--spinner END-->
    <!--spinner END-->

@endsection



@section('tail')

<!-- INTERNAL Select2 js -->
<link href="{{ asset("assets/plugins/select2/select2.min.css") }}" rel="stylesheet" />
<script src="{{ asset("assets/plugins/select2/select2.full.min.js") }}"></script>
<script src="{{ asset("assets/js/select2.js") }}"></script>



<!-- Data table css -->
<link href="{{ asset("assets/plugins/datatable/css/dataTables.bootstrap4.min.css") }}" rel="stylesheet" />
<link href="{{ asset("assets/plugins/datatable/css/buttons.bootstrap4.min.css") }}"  rel="stylesheet">
<link href="{{ asset("assets/plugins/datatable/responsive.bootstrap4.min.css") }}" rel="stylesheet" />

<!-- INTERNAL Data tables -->
<script src="{{ asset("assets/plugins/datatable/js/jquery.dataTables.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/dataTables.bootstrap4.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/dataTables.buttons.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.bootstrap4.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/jszip.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/pdfmake.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/vfs_fonts.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.html5.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.print.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.colVis.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/dataTables.responsive.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/responsive.bootstrap4.min.js") }}"></script>
<script src="{{ asset("assets/js/datatables.js") }}"></script>


<!--- INTERNAL Sweetalert css-->
<link rel="stylesheet" href="{{ asset("assets/sweetalert/sweetalert2.min.css")}}">

<!-- INTERNAL Sweet alert js -->
<script src="{{ asset("assets/sweetalert/sweetalert2.all.min.js")}}"></script>
<script src="{{ asset("assets/sweetalert/sweetalert2.min.js") }}"></script>




<script>

var table;


dataTableCreate();

function dataTableCreate(){

     //DATATABLES PART
     table = $('#staffContentTable').DataTable({
        "autoWidth":false,
        "bDestroy": true,
        language: {
            url: "{{ asset("assets/localization/datatable-tr.json") }}"
        },


    });
}









</script>

@endsection
