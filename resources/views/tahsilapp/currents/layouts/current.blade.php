

<table class="table  table-bordered table-hover" id="currentContentTable">
    <thead>
        <tr>
            <th class="wd-15p border-bottom-0" style="width: 5px">Aktif</th>
            <th class="wd-15p border-bottom-0">Firma Adı</th>
            <th class="wd-20p border-bottom-0">Vergi Dairesi</th>
            <th class="wd-15p border-bottom-0">Vergi No</th>
            <th class="wd-10p border-bottom-0">Şehir</th>
            <th class="wd-25p border-bottom-0">İlçe</th>
            <th class="wd-25p border-bottom-0" style="width: 69px; min-width: 69px;">Gsm</th>
            <th class="wd-25p border-bottom-0">Adres</th>
            <th class="wd-25p border-bottom-0" style=" width: 50px; max-width:50px; text-align: center" > <i class="fa fa-wrench"></i> </th>
        </tr>
    </thead>
    <tbody>

            @include('tahsilapp.currents.layouts.currentTableRow')

    </tbody>
</table>
