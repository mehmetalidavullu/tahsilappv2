@forelse ($currents as $current)

    <tr data-id="{{ $current->id }}">
        <td style="text-align: center">
            <label class="custom-switch">
                <input onchange="statusSwitcher(this)" type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="statusSwitcher" {{ $current->status == 1 ? "checked" : "" }}>
                <span class="custom-switch-indicator"></span>
            </label>
        </td>

        <td><i class="fe fe-eye mr-1" style="cursor:pointer" onclick="showCurrent()" ></i> {{ $current->name }}</td>
        <td>{{ $current->detail->taxoffice }}</td>
        <td>{{ $current->detail->taxnr }}</td>
        <td> @isset($current->detail->district) {{$current->detail->district->city->title}} @endisset </td>
        <td> @isset($current->detail->district) {{ $current->detail->district->title  }} @endisset </td>
        <td>{{ $current->gsm}}</td>
        <td>{{ $current->detail->addr1 }}</td>
        <td style="text-align: center">
            <a class="btn btn-warning btn-sm" id="editbtn" href="javascript:void(0)" onclick="currentEdit(this)" data-toggle="modal" data-target="#currentModal"  data-whatever="Cari Düzenle"  data-data='@json($current)'   style="padding: 3px 7px;">
                <i class="fa fa-edit"></i>
            </a>

            <a class="btn btn-danger btn-sm cariSil" data-id="{{ $current->id }}" onclick="deleteCurrent(this)" style="padding: 3px 8px;"><i class="fa fa-times"></i></a>

        </td>

    </tr>

@empty

@endforelse
