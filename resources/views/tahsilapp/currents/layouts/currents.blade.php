<!--div-->

<div class="page-header">
    <div class="page-leftheader">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/tahsilapp"><i class="fe fe-home mr-2 fs-14"></i>Anasayfa</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="#">Cariler</a></li>
        </ol>
    </div>
</div>


<div class="card">
    <div class="card-header" style="display: inline-block;">
        <div class="card-title">Cariler</div>

        <div  style="float: right">
            <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#currentModal" data-whatever="Cari Ekle" onclick="resetForm()"><i class="fe fe-plus mr-1 fs-14"></i>Yeni Cari Ekle</button>
            <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#currentModal" data-whatever="Cari Düzenle">Cari Düzenle</button> -->
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            @include('tahsilapp.currents.layouts.current')
        </div>
    </div>
</div>
<!--/div-->





