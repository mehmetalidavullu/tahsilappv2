@forelse ($staffs as $staff)

    <tr data-id="{{ $staff->id }}">

        <td style="text-align: center">
            <label class="custom-switch">
                <input onchange="statusSwitcher(this)" type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="statusSwitcher" {{ $staff->status == 1 ? "checked" : "" }}>
                <span class="custom-switch-indicator"></span>
            </label>
        </td>

        <td><i class="fe fe-eye mr-1" style="cursor:pointer" onclick="showCurrent()" ></i> {{ $staff->name }}</td>

        <td>{{ $staff->gsm}}</td>

        <td>{{ $staff->email}}</td>

        <td style="text-align: center">
            <a class="btn btn-warning btn-sm" id="editbtn" href="javascript:void(0)" onclick="currentEdit(this)" data-toggle="modal" data-target="#currentModal"  data-whatever="Personal Düzenle"  data-data='@json($staff)'   style="padding: 3px 7px;">
                <i class="fa fa-edit"></i>
            </a>
            <a class="btn btn-danger btn-sm cariSil" data-id="{{ $staff->id }}" onclick="deleteCurrent(this)" style="padding: 3px 8px;">
                <i class="fa fa-times"></i>
            </a>
        </td>
    </tr>

@empty

@endforelse
