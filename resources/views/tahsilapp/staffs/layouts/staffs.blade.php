<!--div-->

<div class="page-header">
    <div class="page-leftheader">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/tahsilapp"><i class="fe fe-home mr-2 fs-14"></i>Anasayfa</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="#">Personaller</a></li>
        </ol>
    </div>
</div>


<div class="card">
    <div class="card-header" style="display: inline-block;">
        <div class="card-title">Personaller</div>

        <div  style="float: right">
            <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#currentModal" data-whatever="Personal Ekle" onclick="resetForm()"><i class="fe fe-plus mr-1 fs-14"></i>Yeni Personal Ekle</button>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            @include('tahsilapp.staffs.layouts.staff')
        </div>
    </div>
</div>
<!--/div-->





