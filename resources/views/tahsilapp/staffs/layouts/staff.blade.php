

<table class="table  table-bordered table-hover" id="staffContentTable">
    <thead>
        <tr>
            <th class="wd-15p border-bottom-0" style="width: 5px">Aktif</th>
            <th class="wd-15p border-bottom-0">Adı Soyadı</th>
            <th class="wd-25p border-bottom-0" style="width: 69px; min-width: 69px;">Gsm</th>
            <th class="wd-25p border-bottom-0">E-posta</th>
            <th class="wd-25p border-bottom-0" style=" width: 50px; max-width:50px; text-align: center" > <i class="fa fa-wrench"></i> </th>
        </tr>
    </thead>
    <tbody>

            @include('tahsilapp.staffs.layouts.staffTableRow')

    </tbody>
</table>
