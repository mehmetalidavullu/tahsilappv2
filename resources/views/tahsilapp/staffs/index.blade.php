@extends('tahsilapp.layouts.template')

@section('content')


<style>
    .fa-eye{
        float: right;
        position: absolute;
        right: 25px;
        top: 50%;
    }

    #overlay {
    background: #ffffff;
    color: #666666;
    position: fixed;
    height: 100%;
    width: 100%;
    z-index: 5000;
    top: 0;
    left: 0;
    float: left;
    text-align: center;
    padding-top: 25%;
    opacity: 1;
    }

    #staffContentTable td {
        overflow-wrap: anywhere;
    }

</style>



    @include('tahsilapp.staffs.layouts.staffs')

    <!--current modal form-->
    <!--current modal form-->
    <div class="modal fade bd-example-modal-lg" id="currentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="min-width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TİTLE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route("tahsilapp.currents.store") }}" class="currentModalForm needs-validation" id="cariekle" method="post" accept-charset="utf-8" novalidate >

                    @csrf
                    <div class="modal-body">

                            <div class="form-group has-validation">
                                <label>Personal Adı Soyadı</label>
                                <input type="text" class="form-control" id="name" placeholder="Adı Soyadı" name="NAME" required>
                                <div class="invalid-feedback">
                                    Bu alanı doldurmanız gerekmektedir.
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 has-validation">
                                        <label>Gsm</label>
                                        <input type="tel" class="form-control required number phoneformat" id="gsm" placeholder="Telefon Numarası" name="GSM" required>
                                        <div class="invalid-feedback">
                                            Bu alanı doldurmanız gerekmektedir.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 has-validation">
                                        <label>E-posta</label>
                                        <input type="email" class="form-control required email emailcontrol" id="email" placeholder="E-posta adresi" name="EMAIL" required>
                                        <div class="invalid-feedback">
                                            Bu alanı doldurmanız gerekmektedir.
                                        </div>
                                    </div>
                                    <div class="col-md-6 has-validation" >
                                        <label>Şifre</label>
                                        <input type="password" class="form-control required" id="password" placeholder="Şifresi" name="PASSWORD" autocomplete="off" required> <div class="eyes"><i class="fa fa-eye" onclick="showPassword()"></i></div>
                                        <div class="invalid-feedback">
                                            Bu alanı doldurmanız gerekmektedir.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Personal Durumu</label><br>

                                <label for="">Aktif</label>
                                <input type="radio" name="STATUS" id="statusActive" value="1" checked="">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label>Pasif</label>
                                <input type="radio" name="STATUS" id="statusPasif" value="0">

                            </div>

                            <input type="hidden" id="id"  name="id" value="">

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info pull-right"  onclick="currentSaveBtn()">Kaydet</button>
                    </div>

               </form>
            </div>
          </div>

    </div>
    <!--current modal form END-->
    <!--current modal form END-->

    <!--current Info modal form-->
    <!--current Info modal form-->
    <div class="modal fade dtr-bs-modal" id="currentModalInfo" role="dialog"  aria-modal="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Details for Airi Satou</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <table class="table border mb-0 dtr-details" width="100%">
                        <tr data-dt-row="4" data-dt-column="0">
                            <td>Cari Adı:</td>
                            <td id="tdCariAdi">Airi</td>
                        </tr>
                        <tr data-dt-row="4" data-dt-column="8">
                            <td>Gsm:</td>
                            <td id="tdGSM"></td>
                        </tr>
                        <tr data-dt-row="4" data-dt-column="8">
                            <td>E-posta:</td>
                            <td id="tdMail"></td>
                        </tr>
                        <tr data-dt-row="4" data-dt-column="8">
                            <td>Personal Durumu:</td>
                            <td id="tdBayiDurumu"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--current Info modal form END-->
    <!--current Info modal form END-->

    <!--spinner-->
    <!--spinner-->
    <div id="overlay" style="display:none;">
        <div class="spinner4">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!--spinner END-->
    <!--spinner END-->

@endsection



@section('tail')

<!-- INTERNAL Select2 js -->
<link href="{{ asset("assets/plugins/select2/select2.min.css") }}" rel="stylesheet" />
<script src="{{ asset("assets/plugins/select2/select2.full.min.js") }}"></script>
<script src="{{ asset("assets/js/select2.js") }}"></script>



<!-- Data table css -->
<link href="{{ asset("assets/plugins/datatable/css/dataTables.bootstrap4.min.css") }}" rel="stylesheet" />
<link href="{{ asset("assets/plugins/datatable/css/buttons.bootstrap4.min.css") }}"  rel="stylesheet">
<link href="{{ asset("assets/plugins/datatable/responsive.bootstrap4.min.css") }}" rel="stylesheet" />

<!-- INTERNAL Data tables -->
<script src="{{ asset("assets/plugins/datatable/js/jquery.dataTables.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/dataTables.bootstrap4.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/dataTables.buttons.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.bootstrap4.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/jszip.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/pdfmake.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/vfs_fonts.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.html5.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.print.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/js/buttons.colVis.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/dataTables.responsive.min.js") }}"></script>
<script src="{{ asset("assets/plugins/datatable/responsive.bootstrap4.min.js") }}"></script>
<script src="{{ asset("assets/js/datatables.js") }}"></script>


<!--- INTERNAL Sweetalert css-->
<link rel="stylesheet" href="{{ asset("assets/sweetalert/sweetalert2.min.css")}}">

<!-- INTERNAL Sweet alert js -->
<script src="{{ asset("assets/sweetalert/sweetalert2.all.min.js")}}"></script>
<script src="{{ asset("assets/sweetalert/sweetalert2.min.js") }}"></script>




<script>

var table;


dataTableCreate();

function dataTableCreate(){
     //DATATABLES PART
     table = $('#staffContentTable').DataTable({
        "autoWidth":false,
        "bDestroy": true,
        language: {
            url: "{{ asset("assets/localization/datatable-tr.json") }}"
        },


    });
}


function showCurrent(){
    currentData = JSON.parse(event.srcElement.closest("tr").querySelector("#editbtn").dataset.data)
    console.log(currentData)
    $("#currentModalInfo").modal('toggle');


    var titleElem           = $("#currentModalInfo").find(".modal-title");
    var cariAdiElem         = $("#currentModalInfo").find("#tdCariAdi");
    var telefonElem         = $("#currentModalInfo").find("#tdGSM");
    var mailElem            = $("#currentModalInfo").find("#tdMail");
    var bayiDurumuElem      = $("#currentModalInfo").find("#tdBayiDurumu");

    titleElem.text(currentData.name + " personali için detaylar")
    cariAdiElem.text(currentData.name)
    telefonElem.text(currentData.gsm)
    mailElem.text(currentData.email)
    bayiDurumuElem.text(currentData.status == 1 ? "Aktif" : "Pasif")
}

function showPassword(){
    console.log($("#currentModal").find("#password").attr("type"));
    if($("#currentModal").find("#password").attr("type") == "text"){
        $("#currentModal").find("#password").attr("type","password")
    }else{
        $("#currentModal").find("#password").attr("type","text")
    }
}


$('#currentModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var title = button.data('whatever') // Extract info from data-* attributes

    var modal = $(this)
    modal.find('.modal-title').text( title)
})

function resetForm(){
    //RESET FORM
    $("#currentModal").find('.is-invalid').removeClass('is-invalid')
    $("#currentModal").find('.was-validated').removeClass("was-validated");
    $("#currentModal").find(".invalid-feedback").text("Bu alanı doldurmanız gerekmektedir.")
    $('.city-form-control').val("1").trigger('change');
    $("#currentModal").find("#password").attr("type","password")
    $("#currentModal").find("#id").val("");
    $("#currentModal").find("#id").val("");
    $("#cariekle")[0].reset();
}



function fetchCurrent(action, id){
    $('#overlay').fadeIn()
    fetch("{{ route("tahsilapp.staffs.fetchStaffTableContent") }}", {
        method: "POST",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": "{{ csrf_token() }}"
        },
        body: JSON.stringify({id:id})
    }).then(res => res.json())
      .then(res => {


            console.log(res)

            if(action == "update"){
                //console.log("asdsad", id)
                $(`tr[data-id=${id}]`)[0].outerHTML = res.blade;
                table.rows().invalidate()

            }
            else if(action == "add"){
                console.log(res.blade);
               table.row.add( $(res.blade)[0] ).draw(false);
            }


            $('#overlay').fadeOut()
    });
}





function currentSaveBtn(){

    event.preventDefault()

    //FORM VALİDATİON
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var form = document.querySelector('.needs-validation')

    $(form).find('.has-validation').addClass('was-validated')
    $(".currentModalForm").find('#password').prop("required", true )

    //duzenleme fomuysa gerekli değişiklikler yapılır
    if($("#currentModal").find('.modal-title').text()== "Personal Düzenle" ){
        $(".currentModalForm").find('#password').parent().removeClass("was-validated");
        $(".currentModalForm").find('#password').prop("required", false )
    }

    if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
    }else{
        const data = $(".currentModalForm").serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        console.log("data::",data);

        fetch("{{ route("tahsilapp.staffs.store") }}", {
            method: "POST",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-Token": "{{ csrf_token() }}"
            },
            body: JSON.stringify(data)
        })
        .then(res => res.json())
        .then(res => {

            if(res.errors){
                if(res.errors.EMAIL != undefined){
                    $(form).find('.email').addClass('is-invalid')
                    $(form).find('.email').siblings(".invalid-feedback").text(res.errors.EMAIL[0])
                    $(form).find('.email').parent().removeClass("was-validated");
                }
                else{
                    $(form).find('.email').removeClass('is-invalid')
                }
                if(res.errors.GSM != undefined){
                    $(form).find('#gsm').addClass('is-invalid')
                    $(form).find('#gsm').siblings(".invalid-feedback").text(res.errors.GSM[0])
                    $(form).find('#gsm').parent().removeClass("was-validated");
                }
                else{
                    $(form).find('#gsm').removeClass('is-invalid')
                }
                if(res.errors.PASSWORD != undefined){
                    $(form).find('#password').addClass('is-invalid')
                    $(form).find('#password').siblings(".invalid-feedback").text(res.errors.PASSWORD[0])
                    $(form).find('#password').parent().removeClass("was-validated");
                }
                else{
                    $(form).find('#password').removeClass('is-invalid')
                }
            }

            if(res.status == true){
                $('#currentModal').modal('hide');
                if(data.id == ""){
                    fetchCurrent("add", res.id);
                }else{
                    fetchCurrent("update",data.id);
                }
            }

        })

    }

}




function currentEdit(element){

    resetForm();


    var currentData = JSON.parse(element.dataset.data)


    console.log(element.dataset.data)
    console.log(currentData)

    var idElem          = $(".currentModalForm").find("#id");
    var nameElem        = $(".currentModalForm").find("#name");
    var gsm             = $(".currentModalForm").find("#gsm");
    var email           = $(".currentModalForm").find("#email");
    var status          = $(".currentModalForm").find("[name=STATUS]")


    idElem.val(currentData.id)
    nameElem.val(currentData.name);
    gsm.val(currentData.gsm);
    email.val(currentData.email);
    currentData.status == 1 ? status.val(["1"]) : status.val(["0"])

}


function deleteCurrent(element){
    var id = element.dataset.id

    Swal.fire({
        title: 'Silmek istediğinize emin misiniz?',
        text: "Onaylarsanız bu işlemi geri alamazsınız!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet, sil!',
        cancelButtonText: "İptal et"
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
            'Silindi!',
            'Başarıyla silindi.',
            'success'
            )
            $('#overlay').fadeIn()
            fetch("{{ route("tahsilapp.staffs.delete") }}", {
                method: "POST",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": "{{ csrf_token() }}"
                },
                body: JSON.stringify({"id":id})
            })
            .then(res => res.json())
            .then(res => {
                console.log("silme başarılı",res)
                if(res.status){
                    table.row($(element).closest("tr")).remove().draw( false );
                    $('#overlay').fadeOut()
                }
            })
        }
    })

}


function statusSwitcher(element){

    console.log($(element).is(':checked'))

    var data = JSON.parse( $(element).closest("tr").find("#editbtn").attr("data-data") )

    data.status = $(element).is(':checked') == true ? "1" : "0"


    fetch("{{ route("tahsilapp.staffs.changeStatus") }}", {
        method: "POST",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": "{{ csrf_token() }}"
        },
        body: JSON.stringify({ id:data.id, status:data.status })
    })
    .then(res => res.json())
    .then(res => {
        console.log(res);
        if(res.status == true){
            $(element).closest("tr").find("#editbtn").attr("data-data", JSON.stringify(data) )
        }
    })

}





</script>

@endsection
