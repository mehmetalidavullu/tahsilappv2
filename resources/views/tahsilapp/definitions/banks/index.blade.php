<div class="table-responsive" id="banksTable">
    <table class="table table-bordered card-table table-vcenter text-nowrap">
        <thead>
            <tr>
                <th>Banka</th>
                <th>Entegrasyon Bilgi 1</th>
                <th>Entegrasyon Bilgi 2</th>
                <th>Entegrasyon Bilgi 3</th>
                <th>Entegrasyon Bilgi 4</th>
                <th>Entegrasyon Bilgi 5</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($banks as $bank)
            <tr>
                <td scope="row">{{ $bank->name }}</td>
                <td >{{ $bank->bank_info1 }}</td>
                <td >{{ $bank->bank_info2 }}</td>
                <td >{{ $bank->bank_info3 }}</td>
                <td >{{ $bank->bank_info4 }}</td>
                <td >{{ $bank->bank_info5 }}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-outline-light" data-toggle="modal" data-target="#bankUpdate" data-data='@json($bank)' onclick="showModalBanks(this)"><i class="fe fe-edit fs-17"></i></button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade" id="bankUpdate" tabindex="-1" aria-labelledby="bankUpdateLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="bankUpdateLabel">New message</h5>
          <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <form id="js-bankupdate" action="javascript:void(0)">
            <div class="modal-body">
                <input type="hidden" id="update-id" name="id">
                <div class="mb-3">
                <label for="update-name" class="col-form-label">Banka: <span class="text-red">*</span></label>
                <input type="text" class="form-control" id="update-name" name="name">
                </div>

                <div class="mb-3">
                <label for="update-cardtitle" class="col-form-label">Kart İsmi: <span class="text-red">*</span></label>
                <input type="text" class="form-control" id="update-cardtitle" name="card_title">
                </div>

                <div class="mb-3">
                <label for="update-bankinfo1" class="col-form-label">Banka Bilgisi 1:</label>
                <input type="text" class="form-control" id="update-bankinfo1" name="bank_info1">
                </div>

                <div class="mb-3">
                <label for="update-bankinfo2" class="col-form-label">Banka Bilgisi 2:</label>
                <input type="text" class="form-control" id="update-bankinfo2" name="bank_info2">
                </div>

                <div class="mb-3">
                <label for="update-bankinfo3" class="col-form-label">Banka Bilgisi 3:</label>
                <input type="text" class="form-control" id="update-bankinfo3" name="bank_info3">
                </div>

                <div class="mb-3">
                <label for="update-bankinfo4" class="col-form-label">Banka Bilgisi 4:</label>
                <input type="text" class="form-control" id="update-bankinfo4" name="bank_info4">
                </div>

                <div class="mb-3">
                <label for="update-bankinfo5" class="col-form-label">Banka Bilgisi 5:</label>
                <input type="text" class="form-control" id="update-bankinfo5" name="bank_info5">
                </div>

                <div class="mb-3">
                <label for="update-guvenliktipi" class="col-form-label">Güvenlik tipi: <span class="text-red">*</span></label>
                <select class="form-control" id="update-guvenliktipi"  name="security_type">
                    @foreach (config("tahsilapp.security_types") as $item)
                        <option value="{{ $item }}">{{ $item }}</option>
                    @endforeach
                </select>
                </div>


                <div class="mb-3">
                <label for="update-aciklama" class="col-form-label">Açıklama:</label>
                <input type="text" class="form-control" id="update-aciklama" name="description">
                </div>

                <div class="mb-3">
              <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="update-durum" value="1" name="status">
                  <label class="form-check-label" for="update-durum">Aktif</label>
                </div>
            </div>


            <div class="mb-3">
                <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" value="1" id="update-varsayilan" name="isdefault">
                <label class="form-check-label" for="update-varsayilan">
                    Varsayılan
                </label>
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" onclick="updateModalBanks()">Güncelle</button>
        </div>
        </form>
      </div>
    </div>
  </div>
