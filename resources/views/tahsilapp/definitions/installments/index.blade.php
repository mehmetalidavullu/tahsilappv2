<div class="row my-4 mb-6" style="margin: 0 auto;">
    <div class="col-md-4 " >
        <select id="js-bankId" class="form-control custom-select select2">
            @foreach($banks as $bank)
            <option value="{{ $bank->id }}">{{ $bank->name }}</option>
            @endforeach
        </select>
    </div>


        <div class="btn-list" id="installmentButtonList" style="margin-left: auto; margin-right: 10px;">
            <button type="button" class="btn  btn-sm btn-outline-primary" onclick="openEditBtn()" id="editBtn">
                <i class="fe fe-edit mr-2"></i>
                Düzenle
            </button>
            <button type="button" class="btn btn-sm btn-outline-primary" id="saveBtn" onclick="saveBtn()" style="display: none">
                <i class="fe fe-check mr-2"></i>
                Kaydet
            </button>
            <button type="button" class="btn  btn-sm btn-outline-danger"  id="cancelBtn" onclick="closeEditBtn()" style="display: none">
                <i class="fa fa-close mr-2"></i>
                İptal et
            </button>
        </div>

    </div>

    <form id="installments" action="" style="overflow-x: scroll;"></form>

</div>


