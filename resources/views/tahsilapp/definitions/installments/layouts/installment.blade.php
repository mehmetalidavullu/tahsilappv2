
<tr id="i-{{ isset($installment) ? $installment->id : "" }}">

    <td scope="row" class="my-0 align-middle">
		<div class="col-xl-4">
			<div>
				<label class="custom-switch">
                    <div class="material-switch pull-right">
                        <input class="custom-switch-input"  type="checkbox" value="1" name="status" id="status-{{isset($installment) ? $installment->id : "" }}" {{ isset($installment) && $installment->status || !isset($installment)  ? 'checked' : "" }} disabled="disabled" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')" >
                        <label for="status-{{ isset($installment) ? $installment->id : "" }}" class="label-success"></label>
                    </div>
                </label>
            </div>
        </div>
    </td>

    <td scope="row" class="my-0 align-middle">
		<div class="col-xl-4">
			<div>
				<label class="custom-switch">
                    {{-- <div class="material-switch pull-right">
                        <input  type="checkbox" class="custom-switch-input" value="1" name="installment_reflect" id="installment_reflect"  {{ isset($installment) && $installment->installment_reflect == 1 ? "checked" : "" }} disabled="disabled" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')">
                        <span class="custom-switch-indicator custom-radius"></span>
                    </div>
                    --}}
                    <div class="material-switch pull-right">
                        <input class="custom-switch-input2"  type="checkbox" value="1" name="installment_reflect" id="installment_reflect-{{isset($installment) ? $installment->id : "" }}" {{ isset($installment) && $installment->installment_reflect || !isset($installment)  ? 'checked' : "" }} disabled="disabled" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')" >
                        <label for="installment_reflect-{{ isset($installment) ? $installment->id : "" }}" class="label-success"></label>
                    </div>

                </label>
            </div>
        </div>
    </td>

    <td scope="row">
        <input class="form-control" disabled="disabled" type="number" min="0" placeholder="Taksit" width="20px" name="installment_count" id="installment_count" value="{{ isset($installment) ? $installment->installment_count : "" }}" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')">
    </td>

    <td scope="row">
        <input class="form-control "  disabled="disabled" type="number" min="0" placeholder="Ek Taksit Sayısı" name="installment_plus" id="installment_plus" value="{{ isset($installment) ? $installment->installment_plus : "" }}" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')">
    </td>



    <td scope="row">
        <div class="input-group mb-3">
            <input class="form-control" disabled="disabled" type="number" min="0" step="0.10" placeholder="Taksit Oranı" name="installment_rate" id="installment_rate" value="{{ isset($installment) ? $installment->installment_rate : "" }}" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')">
            <div class="input-group-append">
                <span class="input-group-text">%</span>
            </div>
        </div>
    </td>



    <td scope="row">
        <div class="input-group mb-3">
            <input class="form-control" disabled="disabled" type="number" min="0"  step="0.10" placeholder="Üstlenilecek Oran" name="company_reflectange_rate" id="company_reflectange_rate" value="{{ isset($installment) ? $installment->company_reflectange_rate : "" }}" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')">
            <div class="input-group-append">
                <span class="input-group-text">%</span>
            </div>
        </div>
    </td>

    <td scope="row">
        <input class="form-control" type="text" disabled="disabled"  placeholder="Geri Ödeme Plan Kodu" name="geriodemeplankodu" id="geriodemeplankodu" value="{{ isset($installment) ? $installment->geriodemeplankodu : "" }}" onchange="saveInstallment('i-{{ isset($installment) ? $installment->id : '' }}')">
    </td>

    <td scope="row">
        <input type="hidden" name="id" id="id" value="{{ isset($installment) ? $installment->id : "" }}">
        <input type="hidden" name="bank_id" id="bank_id" value="{{ isset($installment) ? $installment->bank_id : $data->id }}">
        @if(isset($installment))

        <a href="javascript:void(0)" class="btn btn-sm btn-outline-light" id="deleteBtn" onclick="deleteInstallmentBtn('i-{{ isset($installment) ? $installment->id : '' }}')"  data-target="#modaldemo5" data-toggle="modal" ><i class="fe fe-trash-2 fs-17"></i></a>

        @else
            <a href="javascript:void(0)" class="btn btn-sm btn-outline-light" id="addBtn" onclick="addBtn()"><i class="fe fe-plus-circle fs-17"></i></a>
        @endif
    </td>
  </tr>
