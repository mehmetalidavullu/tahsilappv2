<style>
    td[scope~="col"]{
        vertical-align: middle !important;
    }
</style>

<table class="table card-table  text-nowrap" style="min-width: 850px;">
    <thead>
    <tr>
        <td scope="col" style="text-align: center">Aktif</td>
        <td scope="col" style="min-width: 95px">Oranı Yansıt</td>
        <td scope="col" >Taksit</td>
        <td scope="col" style="padding-left:0px"> <span style="float:left;"><i class="fa fa-plus mx-2"></i></span> Taksit</td>
        <td scope="col">Taksit Oranı</td>
        <td scope="col">Üstlenilecek Oran</td>
        <td scope="col">Geri Ödeme Kodu</td>
        <td scope="col"></td>
    </tr>
    </thead>
    <tbody>
        @foreach ($data->installments as $installment)
            @include('tahsilapp.definitions.installments.layouts.installment', ['installment' => $installment])
        @endforeach
            @include('tahsilapp.definitions.installments.layouts.installment', ['installment' => null])
    </tbody>
</table>
