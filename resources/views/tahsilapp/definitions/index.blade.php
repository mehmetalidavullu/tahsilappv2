@extends('tahsilapp.layouts.template')

@section('content')


<style>
input, select, textarea{
    color: grey !important;
}
</style>


<!--Page header-->
<div class="page-header">
    <div class="page-leftheader">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/tahsilapp"><i class="fe fe-home mr-2 fs-14"></i>Anasayfa</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="#">Tanımlamalar</a></li>
        </ol>
    </div>
</div>
<!--End Page header-->

<!--Row-->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tanımlamalar</h3>
            </div>
            <div class="card-body">
                <div class="panel panel-primary tabs-style-3">
                    <div class="tab-menu-heading">
                        <div class="tabs-menu ">
                            <!-- Tabs -->
                            <ul class="nav panel-tabs">
                                <li class=""><a href="#tab11" class="active" data-toggle="tab"><i class="fe fe-airplay mr-1"></i> Bankalar</a></li>
                                <li><a href="#tab12" data-toggle="tab"><i class="fe fe-package mr-1"></i>Taksitler</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab11">
                                @include('tahsilapp.definitions.banks.index')
                            </div>
                            <div class="tab-pane" id="tab12">
                                @include('tahsilapp.definitions.installments.index')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Delete confirm modal--->
<div class="modal " id="deleteConfirmModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center p-4">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                    <i class="fe fe-x-circle fs-100 text-danger lh-1 mg-t-20 d-inline-block mb-4"></i>
                <h4 class="text-danger">Dikkat: Kalıcı olarak silmek istediğinize Emin Misiniz!</h4>
                <p class="mg-b-20 mg-x-20">Eğer Onaylarsanız tamamen silinir ve bu işlemi geri alamazsınız.</p>
                <button aria-label="Close" class="btn btn-danger pd-x-25" id="silmeOnayı" type="button">Devam et</button>
            </div>
        </div>
    </div>
</div>
<!--Delete confirm modal End--->


<!---Success confirm modal-->
<div class="modal" id="successConfirmModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center p-4">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button> <i class="fe fe-check-circle fs-100 text-success lh-1 mg-t-20 d-inline-block"></i>
                <h4 class="text-success mt-3">Congratulations!</h4>
                <p class="mg-b-20 mg-x-20">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.</p>
                <button class="btn btn-success pd-x-25" type="button" onclick='$("#successConfirmModal").modal("toggle")'>Tamam</button>
            </div>
        </div>
    </div>

<!--End row-->
@endsection



@section('tail')

<!--notification--->
<script src="{{ asset("assets/plugins/notify/js/jquery.growl.js") }}"></script>
<link href="{{ asset("assets/plugins/notify/css/jquery.growl.css") }}" rel="stylesheet">


<script>
    var bankFormElements = {};




    function showModalBanks(e){
        console.log(e)
        var button = $(e)
        var data = button.data('data')
        console.log(data);

        var modal = $("#bankUpdate")

        var modalTitle = modal.find('.modal-title')
        var name = modal.find('.modal-body #update-name')

        var id = modal.find('.modal-body #update-id')
        var cardTitle = modal.find('.modal-body #update-cardtitle')
        var bankInfo1 = modal.find('.modal-body #update-bankinfo1')
        var bankInfo2 = modal.find('.modal-body #update-bankinfo2')
        var bankInfo3 = modal.find('.modal-body #update-bankinfo3')
        var bankInfo4 = modal.find('.modal-body #update-bankinfo4')
        var bankInfo5 = modal.find('.modal-body #update-bankinfo5')
        var guvenlikTipi = modal.find('.modal-body #update-guvenliktipi')
        var aciklama = modal.find('.modal-body #update-aciklama')
        var durum = modal.find('.modal-body #update-durum')
        var varsayilan = modal.find('.modal-body #update-varsayilan')

        bankFormElements = {}
        bankFormElements =  {
            id:id,
            name:name,
            card_title:cardTitle,
            bank_info1:bankInfo1,
            bank_info2:bankInfo2,
            bank_info3:bankInfo3,
            bank_info4:bankInfo4,
            bank_info5:bankInfo5,
            security_type:guvenlikTipi,
            description:aciklama,
            status:durum,
            isdefault:varsayilan
        }

        modalTitle.text(data.name)
        name.val(data.name)

        id.val(data.id)
        cardTitle.val(data.card_title)
        bankInfo1.val(data.bank_info1)
        bankInfo2.val(data.bank_info2)
        bankInfo3.val(data.bank_info3)
        bankInfo4.val(data.bank_info4)
        bankInfo5.val(data.bank_info5)
        guvenlikTipi.val(data.security_type)
        aciklama.val(data.description)
        durum.prop('checked', data.status == 1 ? true : false)
        varsayilan.prop('checked', data.isdefault == 1 ? true : false)

        //temizle validationu
        banksFormValidationClear()
    }



    //document.querySelector("#js-bankupdate").addEventListener('submit',function(){

    function updateModalBanks(){
        console.log("submitting");
        const data = $("#js-bankupdate").serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        $.ajax({
            url: '{{ route("tahsilapp.bank.update") }}',
            method: 'POST',
            data: data,
            async: false,
            success: function(res){
                console.log(res)
                $("#bankUpdate").modal("hide")
                document.getElementById('tab11').innerHTML = "";
                document.getElementById('tab11').insertAdjacentHTML('afterbegin',res.blade.banks)

                //$("#tab11")[0].innerHTML = res.blade.banks
            },
            error: function(err){
                //önce temizle validationu
                banksFormValidationClear()

                Object.entries(err.responseJSON.errors).forEach( function([key, value]) {
                    console.log(key, value)
                    //console.log(bankFormElements)
                    bankFormElements[key].addClass('is-invalid')
                    bankFormElements[key].after(`
                    <div class="invalid-feedback">
                        ${value[0]}
                    </div>
                    `)
                })
            }
        })
    }



    function banksFormValidationClear(){
        Object.entries(bankFormElements).forEach( function([key, value]) {
            bankFormElements[key].removeClass('is-invalid')
            bankFormElements[key].find("#invalid-feedback").remove()
        });
    }


    //Installment SECTİON------------------
    //--------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------


        var installmentData = {}
        var deletedInstallmentData = [];
        var newInstallmentIndex = -1;

        $("#js-bankId").trigger("change");

        $("#js-bankId").change(getInstallments);


        //bank id'ye göre installmentler dbden alınıyor
        function getInstallments(e){

            const id = e.target.value

            fetch("{{ route("tahsilapp.installment.installments") }}", {
                method: "POST",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": "{{ csrf_token() }}"
                },
                body: JSON.stringify({id:id})
            })
            .then(res => res.json())
            .then(res => {
                document.getElementById('installments').innerHTML = res.blade.installments

                $("#installmentButtonList #editBtn").css("display","inline")
                $("#installmentButtonList #saveBtn").css("display","none")
                $("#installmentButtonList #cancelBtn").css("display","none")
                $("#installments").find('input').prop("disabled", true);
                $("#installments").find('a').css('visibility', 'hidden')
                $("#i-").hide();
                installmentData = {};
                deletedInstallmentData = []

            });


        }

        //bank_id'ye göre installmentler db'ye kaydedilir yada update edilir
        function setInstallment(data){

            deleteInstallment(deletedInstallmentData)

            fetch("{{ route("tahsilapp.installment.store") }}", {
                method: "POST",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": "{{ csrf_token() }}"
                },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then(res => {
                if(res.errors != undefined){
                    $.growl.error({
                        title: "Hata!!",
                        message: res.errors[Object.keys(res.errors)[0]][0]
                    });
                }
                else{
                    closeEditBtn();
                    $("#js-bankId").trigger("change");
                    $("#successConfirmModal").find("h4").text("Başarılı.")
                    $("#successConfirmModal").find("p").text("Yaptığınız değişiklikler başarıyla kaydedilmiştir..")
                    $("#successConfirmModal").modal("toggle")
                }
            });
        }

        //formdan yeni eklenecek installment verileri alında
        function saveInstallment(e){
            var id = $("#" + e + ' #id').val();

            if(id==""|| id==undefined){
                if(event.srcElement.closest('tr').id != "i-"){
                    id = event.srcElement.closest('tr').id.substring(event.srcElement.closest('tr').id.indexOf('-') + 1) ;
                    //console.log(id)

                }else{
                    return false;
                }
            }

            //taksit oranı üstelinecek orandan büyük olamaz
            if($("#i-" + id + ' #installment_rate').val() > 0 ){

                $("#i-" + id + ' #company_reflectange_rate').attr("max", $("#i-" + id + ' #installment_rate').val())

                if( $("#i-" + id + ' #company_reflectange_rate').val() > $("#i-" + id + ' #installment_rate').val() ){
                    $("#i-" + id + ' #company_reflectange_rate').val($("#i-" + id + ' #installment_rate').val())
                }
            }


            var prefix = "#i-" + id

                installmentData[id] = {},
                installmentData[id].id = $(prefix + ' #id').val(),
                installmentData[id].bank_id = $(prefix + ' #bank_id').val(),
                installmentData[id].installment_count = $(prefix + ' #installment_count').val(),
                installmentData[id].installment_plus = $(prefix + ' #installment_plus').val(),
                installmentData[id].installment_rate = $(prefix + ' #installment_rate').val(),
                installmentData[id].company_reflectange_rate = $(prefix + ' #company_reflectange_rate').val(),
                installmentData[id].status = $(prefix + ` #status-${id}`).is(':checked') ? 1: 0;
                installmentData[id].installment_reflect = $(prefix +` #installment_reflect-${id}`).is(':checked') ? 1: 0;
                installmentData[id].geriodemeplankodu = $(prefix + ' #geriodemeplankodu').val(),

            console.log(installmentData);
        }


        function saveBtn(){
            setInstallment(installmentData);
        }


        function deleteInstallmentBtn(e){

            var id = $("#" + e + ' #id').val();

            if(id==""){
                if(event.srcElement.closest('tr').id != "i-"){
                    id = event.srcElement.closest('tr').id.substring(event.srcElement.closest('tr').id.indexOf('-') + 1) ;

                }else{
                    return false;
                }
            }

            var prefix = "#" + e

            delete installmentData[id]

            deletedInstallmentData.push(id),

            $('#deleteConfirmModal').modal('toggle');

            $('#silmeOnayı').one("click", function(){

                $('#deleteConfirmModal').modal('hide');

                if($("#installments").find(`#i-${id}`)[0] != undefined){
                    $("#installments").find(`#i-${id}`)[0].outerHTML = "";
                }
            })

        }

        function deleteInstallment(data){
            fetch("{{ route("tahsilapp.installment.destroy") }}", {
                method: "DELETE",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": "{{ csrf_token() }}"
                },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then(res => {
                if(res.errors != undefined){
                }
                else{
                }
            });
        }


        function openEditBtn(){
            $("#installmentButtonList #editBtn").css("display","none")
            $("#installmentButtonList #saveBtn").css("display","inline")
            $("#installmentButtonList #cancelBtn").css("display","inline")
            $("#installments").find('input').prop("disabled", false);
            $("#i-").show();
            $("#installments").find("#i-").find("input").prop("disabled",true)
            $("#installments").find('a').show();
            $("#installments").find('a').css('visibility', 'visible')
            installmentData = {};
            deletedInstallmentData = []
        }

        function closeEditBtn(){

            $("#js-bankId").trigger("change");
        }


        function addBtn(){

            var clonedElem = $("#installments").find("#i-").clone()

            $("#installments").find("#i-").attr("id", "i-"+newInstallmentIndex.toString())

            $("#installments").find("#i-"+newInstallmentIndex).find("input").attr("disabled", false)

            //console.log($("#installments").find("#i-"+newInstallmentIndex).find(".custom-switch-input"))
            $("#installments").find("#i-"+newInstallmentIndex).find(".custom-switch-input").attr('id', "status-"+ newInstallmentIndex.toString() )
            $("#installments").find("#i-"+newInstallmentIndex).find(".custom-switch-input").siblings('label').attr('for', "status-"+ newInstallmentIndex.toString() )


            $("#installments").find("#i-"+newInstallmentIndex).find(".custom-switch-input2").attr('id', "installment_reflect-"+ newInstallmentIndex.toString() )
            $("#installments").find("#i-"+newInstallmentIndex).find(".custom-switch-input2").siblings('label').attr('for', "installment_reflect-"+ newInstallmentIndex.toString() )

            $("#installments").find("#i-"+newInstallmentIndex).find("input").first().trigger('change');

            $("#installments").find("#i-"+newInstallmentIndex.toString()).find("a")[0].outerHTML = `<a href="javascript:void(0)" class="btn btn-sm" id="deleteBtn" onclick="deleteInstallmentBtn('i-${newInstallmentIndex}')"  data-target="#modaldemo5" data-toggle="modal" ><i class="fe fe-trash-2 fs-17"></i></a>`

            clonedElem.attr("id","i-")
            clonedElem.find("input").attr("disabled",true)
            $("#installments").find("#i-"+newInstallmentIndex.toString()).after(clonedElem)

            newInstallmentIndex--
        }




        //Installment section END------------------

</script>
@endsection
