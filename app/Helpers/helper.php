<?php

if(!function_exists('p')){
    function p($permission){
        $user = auth('tahsilapp')->user();

        return $user->hasPermissionTo($permission);
    }
}

if(!function_exists('user')){
    function user(){
        $user = auth('tahsilapp')->user();

        return $user;
    }
}



if(!function_exists('getCurrentUserRole')){
    function getCurrentUserRole(){
        $role = auth()->guard('tahsilapp')->user()->roles->pluck('name')->first();
        return $role;
    }
}
