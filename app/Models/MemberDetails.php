<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\District;

class MemberDetails extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function district(){
        return $this->belongsTo(District::class, "district_id");
    }
}
