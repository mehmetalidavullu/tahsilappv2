<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function installments(){
        return $this->hasMany(Installment::class);
    }
}
