<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\District;

class City extends Model
{


    public function districts(){
        return $this->hasMany(District::class,'city_id');
    }

    public function  scopeFindDistricts($query, $id){
        //City::find(1)->districts()->get();
        return $query->find($id)->districts();
    }

}
