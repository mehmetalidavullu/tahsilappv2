<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  auth("tahsilapp")->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $securitytypes = implode(",", config('tahsilapp.security_types'));

        return [
            "id"=> "required|integer",
            "name"=> "required|string|max:250",
            "card_title"=> "required|string|max:250",
            "bank_info1"=> "nullable|string|max:250",
            "bank_info2"=> "nullable|string|max:250",
            "bank_info3"=> "nullable|string|max:250",
            "bank_info4"=> "nullable|string|max:250",
            "bank_info5"=> "nullable|string|max:250",
            "description"=> "required|string|max:250",
            "security_type"=> "required|string|in:".$securitytypes,
            "status"=> "nullable|integer",
            "isdefault"=> "nullable|integer",
        ];
    }

    public function attributes()
    {
        return [
            'name' => "Banka adı",
            'card_title' => "Kart ismi",
            'bank_info1'=> "Banka bilgisi 1",
            'bank_info2' => "Banka bilgisi 2",
            'bank_info3'=> "Banka bilgisi 3",
            'bank_info4' => "Banka bilgisi 4",
            'bank_info5' => "Banka bilgisi 5",
            'description' => "Açıklama",
            'security_type' => "Güvenlik tipi",
            'status' => "Aktif",
            'isdefault'=> "Varsayılan",
        ];
    }
}
