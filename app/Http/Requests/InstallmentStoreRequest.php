<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstallmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  auth("tahsilapp")->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "*.id"=>"nullable|integer",
            "*.bank_id"=> "required|integer",
            "*.installment_count" => "required|integer",
            "*.installment_plus" => "nullable|integer",
            "*.installment_reflect" => "required|integer|in:0,1",
            "*.installment_rate" => "required",
            "*.company_reflectange_rate" => "nullable",
            "*.status" => "nullable|boolean",
            "*.geriodemeplankodu"=> "required"
        ];
    }


    public function attributes()
    {
        return [
            '*.company_reflectange_rate' => 'Üstlenilecek oran',
            '*.geriodemeplankodu' => 'Geri ödeme Kodu',
            '*.installment_count'=> 'Taksit sayısı',
            '*.installment_plus' => 'Artı taksit sayısı',
            '*.installment_rate' => 'Taksit oranı',
            '*.installment_reflect' => 'Oranı yansıt',
            "*.status" => "Durum",
        ];
    }

}
