<?php

namespace App\Http\Requests;

use App\Models\Member;
use Illuminate\Foundation\Http\FormRequest;

class CurrentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  auth("tahsilapp")->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if( !empty(request()->input('id')) ){

            //email part
            $email =  Member::find(request()->input('id'))->email;
            $gsm =  Member::find(request()->input('id'))->gsm;

            $validate = [];

            if( $email != request()->input('EMAIL')  ){
                $validate["EMAIL"] =  "unique:members,email";
            }

            if ($gsm != request()->input('GSM')){
                $validate["GSM"] = "unique:members,gsm";
            }

            if(!empty(request()->input('PASSWORD'))){
                $validate["PASSWORD"] = "min:4";
            }

            return $validate;
        }
        else{
            return [
                "EMAIL" => "unique:members,email",
                "GSM" => "unique:members,gsm",
                "PASSWORD" => "min:4",
            ];
        }


    }

    public function attributes()
    {
        return [
            "EMAIL"=> "E-posta",
            "GSM" => "Gsm",
            "PASSWORD"=> "Şifre"
        ];
    }
}
