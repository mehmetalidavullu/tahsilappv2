<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bank;

class DefinitionController extends Controller
{
    public function index(){
        $banks = Bank::with("installments")->orderBy("name", "asc")->get();

        return view('tahsilapp.definitions.index', compact('banks'));
    }
}
