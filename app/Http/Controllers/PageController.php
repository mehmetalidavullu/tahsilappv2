<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        return view('tahsilapp.pages.index');
    }


    public function create(){
        return view('tahsilapp.pages.create');
    }
}
