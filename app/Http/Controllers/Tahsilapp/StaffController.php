<?php

namespace App\Http\Controllers\Tahsilapp;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Http\Requests\StaffStoreRequest;

class StaffController extends Controller
{

    public function index(){
        $staffs = Member::role('sahapersonali')->orderBy('id', "asc")->get();

        return view('tahsilapp.staffs.index',compact('staffs') );
    }

    public function store(StaffStoreRequest $request){
        $data = [
            "name" => request()->input('NAME'),
            "email" => request()->input('EMAIL'),
            "status" => request()->input('STATUS'),
            "gsm" => request()->input('GSM'),
            "top_id" =>request()->input('BAYI_ID'),
        ];

        if( empty(request()->input('PASSWORD')) ){
            $member= Member::updateOrCreate(
                ["id" => request()->input('id')],
                $data
            );
        }else{
            $data["password"] = bcrypt(request()->input('PASSWORD'));

            $member= Member::updateOrCreate(
                ["id" => request()->input('id')],
                $data
            );
        }

        $member->assignRole("sahapersonali");

        return response()->json([
            "status"=> true,
            "id" => $member->id,
            "name"=>$member->name,
        ]);
    }

    public function fetchStaffTableContent(){
        $staffs = Member::role('sahapersonali')->orderBy('id', "asc")->get();

        return response()->json(
            ["blade"=> view('tahsilapp.staffs.layouts.staffTableRow', compact('staffs'))->render() ], 200);
    }

    public function destroy(){
        Member::destroy(request()->input('id'));

        return response()->json([
            "status"=> true,
        ]);
    }

    public function changeStatus(){
        $member =  Member::find(request()->input('id'));
        $member->status = request()->input('status');
        $member->save();

        return response()->json([
            "status"=> true,
        ]);
    }


}
