<?php

namespace App\Http\Controllers\Tahsilapp;

use App\Http\Controllers\Controller;
use App\Http\Requests\BankUpdateRequest;
use App\Models\Bank;

class BankController extends Controller
{
    public function update(BankUpdateRequest $request){
        $request->merge(['status'=>$request->has('status')]);
        $request->merge(['isdefault'=>$request->has('isdefault')]);

        $data = tap(Bank::findOrFail($request->id))->update($request->all());

        return response()->json([
            "data"=> $data,
            "blade"=> $this->index()
        ],200);
    }


    public function index(){

        $banks = Bank::with("installments")->orderBy("name", "asc")->get();

        return array("banks"=> view('tahsilapp.definitions.banks.index',compact('banks'))->render());


    }
}
