<?php

namespace App\Http\Controllers\Tahsilapp\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{

    public function __construct(){

        $this->middleware(function($request, $next){
            if(Auth::guard('tahsilapp')->check() && $request->route()->getName() != "tahsilapp.logout"){
                return redirect()->route('tahsilapp.dashboard');
            }
            return $next($request);
        });

    }

    public function index(){

        return view('tahsilapp.auth.login');
    }


    public function login(Request $request){

        $request->validate([
            'username'=> 'required|max:250',
            'password'=> 'required|min:4',
        ]);

        $request->merge([
            'remember_me'=>$request->has('remember_me'),
        ]);

        $username = $request->username;
        $password = $request->password;

        if(filter_var($username, FILTER_VALIDATE_EMAIL)){
            Auth::guard('tahsilapp')->attempt([
                'email' => $username,
                'password'=> $password
            ], $request->remember_me);
        }
        else{
            Auth::guard('tahsilapp')->attempt([
                'gsm' => $username,
                'password'=> $password
            ], $request->remember_me);
        }

        if(Auth::guard('tahsilapp')->check()){
            return redirect()->intended(route('tahsilapp.dashboard'));
        }

        return redirect()->back()->withErrors([
            'error'=> 'Oturum açılamadı'
        ]);
    }

    public function logout(Request $request){
        Auth::guard('tahsilapp')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
        return redirect()->route('tahsilapp.login');
    }
}
