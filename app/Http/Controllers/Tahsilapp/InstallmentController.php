<?php

namespace App\Http\Controllers\Tahsilapp;

use App\Http\Controllers\Controller;
use App\Http\Requests\InstallmentStoreRequest;
use App\Models\Bank;
use App\Models\Installment;
use Illuminate\Support\Facades\Request;

class InstallmentController extends Controller
{
    public function installmentsByBank(){
        $data = Bank::with('installments')->findOrFail(request()->id);

        return response()->json([
            "blade"=>[
                "installments"=> view('tahsilapp.definitions.installments.layouts.installments',compact('data'))->render()
            ]
        ]);
    }

    public function store(InstallmentStoreRequest $request){


       foreach (request()->all() as $key => $installment) {

            Installment::updateOrCreate(
                [
                    "id"=> $installment['id'],
                    "bank_id" => $installment["bank_id"],
                ],
                [
                    "company_reflectange_rate"=>$installment["company_reflectange_rate"],
                    "geriodemeplankodu"=> $installment["geriodemeplankodu"],
                    "installment_count"=> $installment["installment_count"],
                    "installment_plus"=> $installment["installment_plus"],
                    "installment_rate"=> $installment["installment_rate"],
                    "installment_reflect" => $installment["installment_reflect"],
                    "status" => $installment["status"],
                ]
            );
        }

        return response()->json([
            "status"=> true
        ]);
    }


    public function destroy(){


        foreach (request()->all() as $key => $value) {

            if( ! str_contains($value, '-')  ){
                Installment::find($value)->delete();
            }

        }

        return response()->json([
            "status"=> true
        ]);

    }
}
