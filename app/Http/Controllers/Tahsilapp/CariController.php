<?php

namespace App\Http\Controllers\Tahsilapp;

use App\Http\Controllers\Controller;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Models\City;

use App\Http\Requests\CurrentStoreRequest;

class CariController extends Controller
{

    public function index(){
        $allcurrents = Member::role('cari')->orderBy('id', "asc")->get();
        $bayiler = $allcurrents->pluck('name','id');
        $currents = Member::role('cari')->with('detail.district.city')->orderBy('id', "asc")->get();

        $cities = City::all();

        return view('tahsilapp.currents.index',compact('cities', 'currents', 'allcurrents', 'bayiler') );
    }

    public function store(CurrentStoreRequest $request){
        $data = [
            "name" => request()->input('NAME'),
            "email" => request()->input('EMAIL'),
            "status" => request()->input('STATUS'),
            "gsm" => request()->input('GSM'),
            "top_id" =>request()->input('BAYI_ID'),
        ];

        if( empty(request()->input('PASSWORD')) ){
            $member= Member::updateOrCreate(
                ["id" => request()->input('id')],
                $data
            );
        }else{
            $data["password"] = bcrypt(request()->input('PASSWORD'));

            $member= Member::updateOrCreate(
                ["id" => request()->input('id')],
                $data
            );
        }

        $member->detail()->updateOrCreate(
            [
                "member_id"=>$member->id
            ],
            [
                "addr1" => request()->input('ADDR1'),
                "district_id" => request()->input('DİSTRİCT'),
                "mersisnr" => request()->input('MERSISNR'),
                "taxnr" => request()->input('TAXNR'),
                "taxoffice" => request()->input('TAXOFFICE'),
                "tckno" => request()->input('TCKNO'),
                "telnrs" => request()->input('TELNRS'),
                "traderegisternr" => request()->input('TRADEREGISTERNR'),
            ]
        );

        $member->assignRole("cari");

        return response()->json([
            "status"=> true,
            "id" => $member->id,
            "name"=>$member->name,
        ]);
    }

    public function fetchCurrentTableContent(){
        $currents = Member::with('detail.district.city')->where("id", request()->input('id') )->get();

        return response()->json(
            ["blade"=> view('tahsilapp.currents.layouts.currentTableRow', compact('currents'))->render() ], 200);
    }

    public function destroy(){
        Member::destroy(request()->input('id'));

        return response()->json([
            "status"=> true,
        ]);
    }

    public function changeStatus(){
        $member =  Member::find(request()->input('id'));
        $member->status = request()->input('status');
        $member->save();

        return response()->json([
            "status"=> true,
        ]);

    }


}
