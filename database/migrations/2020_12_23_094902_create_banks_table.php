<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('integration_id')->unique();
            $table->string('name');
            $table->string('card_title')->nullable();
            $table->string('bank_info1')->nullable();
            $table->string('bank_info2')->nullable();
            $table->string('bank_info3')->nullable();
            $table->string('bank_info4')->nullable();
            $table->string('bank_info5')->nullable();
            $table->enum('security_type',['regular', '3d_pay', '3d'])->default('3d');
            $table->string('description')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('isdefault')->default(0);
            $table->string('erpbankakodu')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
