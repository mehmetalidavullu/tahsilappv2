<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('member_id');
            $table->string('code',50)->nullable();
            $table->text('addr1')->nullable();
            $table->text('addr2')->nullable();
            $table->integer('district_id')->nullable();
            $table->string('telnrs',50)->nullable();
            $table->string('incharge',100)->nullable();
            $table->string('inchargemail',100)->nullable();
            $table->string('website',250)->nullable();
            $table->float('debit')->nullable();
            $table->float('credit')->nullable();
            $table->float('balance')->nullable();
            $table->integer('ispercomp')->nullable();
            $table->string('taxoffice',50)->nullable();
            $table->string('taxnr',11)->nullable();
            $table->string('tckno',11)->nullable();
            $table->integer('accepteinv')->nullable();
            $table->string('sectormain',50)->nullable();
            $table->string('sectorsub',50)->nullable();
            $table->string('mersisnr',50)->nullable();
            $table->string('traderegisternr',50)->nullable();

            $table->foreign('member_id')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_details');
    }
}
