<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Member;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('members')->truncate();
        DB::table('member_details')->truncate();

        Member::create([
            'eofguid' => 0,
            'status' => 1,
            'name' => 'NT Bilgi',
            'email' => 'destek@ntbilgi.com',
            'gsm' =>'05555555555',
            'password' => bcrypt('admin'),
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


    }
}
