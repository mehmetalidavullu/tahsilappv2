<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('banks')->truncate();

        $banks = [
            [
                'integration_id' => 5,
                'name' => 'halkbank',
                'card_title' => 'visa',
                'bank_info1' => 'sadasdsad',
                'bank_info2' => 'asdasdsad',
                'bank_info3' => 'asdasdsad',
                'bank_info4' => 'asdasdasd',
                'bank_info5' => 'zxcsdfasd',
                'security_type' => '3d',
                'description' => 'username-password-storekey-merchantId-enc',
                'status' => 1,
                'isdefault' => 0,
            ],
            [
                'integration_id' => 6,
                'name' => 'ziraatbank',
                'card_title' => 'visa',
                'bank_info1' => 'sadasdsad',
                'bank_info2' => 'asdasdsad',
                'bank_info3' => 'asdasdsad',
                'bank_info4' => 'asdasdasd',
                'bank_info5' => 'zxcsdfasd',
                'security_type' => '3d',
                'description' => 'username-password-storekey-merchantId-enc',
                'status' => 1,
                'isdefault' => 1,
            ],
            [
                'integration_id' => 7,
                'name' => 'işbankası',
                'card_title' => 'visa',
                'bank_info1' => 'sadasdsad',
                'bank_info2' => 'asdasdsad',
                'bank_info3' => 'asdasdsad',
                'bank_info4' => 'asdasdasd',
                'bank_info5' => 'zxcsdfasd',
                'security_type' => '3d',
                'description' => 'username-password-storekey-merchantId-enc',
                'status' => 1,
                'isdefault' => 0,
            ],
        ];

        DB::table('banks')->insert($banks);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
