<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Member;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permissions')->truncate();
        DB::table('roles')->truncate();
        DB::table('model_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('role_has_permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');





        Role::create(['guard_name'=>'tahsilapp', 'name' => 'superadmin']);
        Role::create(['guard_name'=>'tahsilapp', 'name' => 'yonetici']);
        Role::create(['guard_name'=>'tahsilapp', 'name' => 'muhasebe']);
        Role::create(['guard_name'=>'tahsilapp', 'name' => 'sahapersonali']);
        Role::create(['guard_name'=>'tahsilapp', 'name' => 'cari']);
        Role::create(['guard_name'=>'tahsilapp', 'name' => 'bayi']);




        $user = Member::first();
        $user->assignRole('superadmin');

        //role oluşturma
        //$role = Role::create(['guard_name'=>'tahsilapp', 'name' => 'admin']);
        //yetki oluşturma
         //Permission::create(['guard_name'=>'tahsilapp','name' => 'product_management']);
         //Permission::create(['guard_name'=>'tahsilapp', 'name' => 'category_management']);
         //Permission::create(['guard_name'=>'tahsilapp', 'name' => 'order_management']);

       // $user_management = Permission::create(['guard_name'=>'tahsilapp','name' => 'user_management']);

        //role yetki atama
        //$role->givePermissionTo($user_management);

        //kullanıcya role atama
        //$user->assignRole($role);

        //kullanıcıya yetki tanımlama
        //$user->givePermissionTo(['product_management', 'category_management']);

    }
}
