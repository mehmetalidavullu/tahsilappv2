## Kurulum

.env.example dosyasını .env olarak kopyalayın ve veritabanı vb. düzenlemeleri yapın.

## Çalıştırılacak komutlar
1. composer install
2. php artisan key:generate
3. php artisan migrate
4. php artisan:db seed

Ardından php artisan serve ile uygulama başlatılabilir.
