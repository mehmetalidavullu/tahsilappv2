<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Tahsilapp\Auth\LoginController;
use App\Http\Controllers\Tahsilapp\BankController;
use App\Http\Controllers\Tahsilapp\DashboardController;
use App\Http\Controllers\Tahsilapp\InstallmentController;
use App\Http\Controllers\DefinitionController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Tahsilapp\CariController;
use App\Http\Controllers\Tahsilapp\StaffController;
use App\Models\City;

Route::group(['prefix'=> 'tahsilapp', 'as' => 'tahsilapp.'], function(){

    Route::get('login',[LoginController::class,'index'])->name('login');
    Route::get('logout',[LoginController::class,'logout'])->name('logout');

    Route::post('login',[LoginController::class,'login']);

    Route::group(['middleware' => 'isTahsilAppUser'], function () {

        Route::get('/',[DashboardController::class, 'index'])->name('dashboard');
        Route::get('/definitions',[DefinitionController::class, 'index'])->name('definition');
        Route::get('/currents', [CariController::class, 'index'])->name('current');
        Route::get('/staffs', [StaffController::class, 'index'])->name('staff');
        Route::get('/pages', [PageController::class, 'index'])->name('pages');


        Route::post('/districts',function(){
             return response()->json( [City::findDistricts(request()->input('id'))->get()], 200);
        })->name("districts");

        Route::group(['prefix' => 'banks'], function () {
            Route::post('/update',[BankController::class, 'update'])->name('bank.update');
        });

        Route::group(['prefix'=>'installments'], function(){
            Route::post('/installmentsByBank', [InstallmentController::class, 'installmentsByBank'])->name('installment.installments');
            Route::post('/store', [InstallmentController::class, 'store'])->name('installment.store');
            Route::delete('/delete', [InstallmentController::class, 'destroy'])->name('installment.destroy');
        });

        Route::group(['prefix'=> 'currents'], function(){
            Route::post('/store', [CariController::class, 'store'])->name('currents.store');
            Route::post('/fetchCurrentTableContent', [CariController::class, 'fetchCurrentTableContent'])->name('currents.fetchCurrentTableContent');
            Route::post('/delete', [CariController::class, 'destroy'])->name('currents.delete');
            Route::post('/status', [CariController::class, 'changeStatus'])->name('currents.changeStatus');
        });

        Route::group(['prefix'=> 'staffs'], function(){
            Route::post('/store', [StaffController::class, 'store'])->name('staffs.store');
            Route::post('/fetchStaffTableContent', [StaffController::class, 'fetchStaffTableContent'])->name('staffs.fetchStaffTableContent');
            Route::post('/delete', [StaffController::class, 'destroy'])->name('staffs.delete');
            Route::post('/status', [StaffController::class, 'changeStatus'])->name('staffs.changeStatus');
        });


        Route::group(['prefix'=> 'pages'], function(){
            Route::get('/create', [PageController::class, 'create'])->name('pages.create');

        });


    });
});



